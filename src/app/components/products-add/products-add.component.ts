import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validator, Validators} from '@angular/forms';
import {IProduct} from '../../domain/iproduct';
import {ProductsService} from '../../services/products.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-products-add',
  templateUrl: './products-add.component.html',
  styleUrls: ['./products-add.component.scss']
})
export class ProductsAddComponent implements OnInit {

  form: FormGroup;

  constructor(private  _fb: FormBuilder,
              private _service: ProductsService,
              private _router: Router) {
  }

  ngOnInit() {
    this.form = this._fb.group(
      {
        id: new FormControl('', Validators.required),
        nom: new FormControl('', Validators.required),
        prix: new FormControl('', [Validators.required, Validators.pattern('[+-]?[0-9.]+')])
      }
    );
  }

  addProduct(product: IProduct) {
    this._service.addProduct(product).subscribe(
      resp => this._router.navigateByUrl('/list'),
      error1 => console.log(`Attention il y a erreur ${error1}`)
    );
  }

}
