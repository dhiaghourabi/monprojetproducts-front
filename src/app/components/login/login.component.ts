import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {validate} from 'codelyzer/walkerFactory/walkerFn';
import {IUser} from '../../domain/iuser';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form: FormGroup;


  constructor(private  _fb: FormBuilder,
              private  _router: Router) {
  }

  ngOnInit() {
    this.form = this._fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  login(user: IUser) {
    console.log('Vous avez saisi l\'utilisateur :', user);
    this._router.navigateByUrl('/');
  }

}
