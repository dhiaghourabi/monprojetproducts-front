import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';


import {AppComponent} from './app.component';
import {ProductsListComponent} from './components/products-list/products-list.component';
import {NavbarComponent} from './components/navbar/navbar.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FooterComponent} from './components/footer/footer.component';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule, Routes} from '@angular/router';
import {WelcomeComponent} from './components/welcome/welcome.component';
import {NotFoundComponent} from './components/not-found/not-found.component';
import {ProductsAddComponent} from './components/products-add/products-add.component';
import {ProductsDetailComponent} from './components/products-detail/products-detail.component';
import {ProductsDetailGuard} from './guards/products-detail.guard';
import {ReactiveFormsModule} from '@angular/forms';
import {LoginComponent} from './components/login/login.component';

const routes: Routes = [
  {path: '', component: WelcomeComponent},
  {path: 'list', component: ProductsListComponent},
  {path: 'add', component: ProductsAddComponent},
  {
    path: 'detail/:id',
    canActivate: [ProductsDetailGuard],
    component: ProductsDetailComponent
  },
  {path: 'login', component: LoginComponent},
  {path: '**', component: NotFoundComponent},

];

@NgModule({
  declarations: [
    AppComponent,
    ProductsListComponent,
    NavbarComponent,
    FooterComponent,
    WelcomeComponent,
    NotFoundComponent,
    ProductsAddComponent,
    ProductsDetailComponent,
    LoginComponent,
  ],
  imports: [
    BrowserModule,
    NgbModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
