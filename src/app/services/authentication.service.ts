import {Injectable} from '@angular/core';
import {IUser} from '../domain/iuser';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor() {
  }

  loggedIn: boolean = false;

  authenticate(user: IUser): string {
    if (user.username === 'dhia' && user.password === '123') {
      this.loggedIn = true;
      const token: string = '0a12svf,skeààvf5445$$';
      localStorage.setItem('TOKEN', token);
      return token;
    }
  }

  logout() {
    if (localStorage.getItem('TOKEN')) {
      this.loggedIn = false;
      localStorage.removeItem('TOKEN');
    }
  }

  isAuthenticated() {
    return this.loggedIn;
  }
}
