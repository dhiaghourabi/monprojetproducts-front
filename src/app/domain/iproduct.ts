export interface IProduct {
  id: string;
  nom: string;
  prix: number;
}
